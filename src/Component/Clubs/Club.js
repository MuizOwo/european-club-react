import React from 'react'


const Club = ({Tittle, Description}) => {
      return (
            <div>
                  <div className="row">
                  <div className="col s12 m6">
                        <div className="card blue-grey darken-1">
                        <div className="card-content white-text">
                        <span className="card-title">{Tittle}  <i class="far fa-edit"></i></span> 
                        <div className='divider'></div>
                        <p>{Description}</p>
                        </div>
                        <div className="card-action">
                        <a href="google.com">Visit {Tittle} Website</a>
                        <i class="far fa-trash-alt red-text "></i>
                        </div>
                        </div>
                  </div>
                  </div>
            </div>
      )
}

export default Club