import React, {Component} from 'react'
import Club from './Club.js'

class Clubs extends Component {

  state={
    clubs:[
    {
      Tittle: 'Barcelona',
      Description:'Founded in 1899 by a group of Swiss, Spanish, English, and Catalan footballers led by Joan Gamper, the club has become a symbol of Catalan culture and Catalanism, hence the motto "Més que un club" ("More than a club"). Unlike many other football clubs, the supporters own and operate Barcelona. It is the fourth-most valuable sports team in the world, worth $4.06 billion, and the world\'s richest football club in terms of revenue, with an annual turnover of €840,8 million'
    },
    {
      Tittle: 'Real Madrid',
      Description:'Founded on 6 March 1902 as Madrid Football Club, the club has traditionally worn a white home kit since inception. The word real is Spanish for "royal" and was bestowed to the club by King Alfonso XIII in 1920 together with the royal crown in the emblem. The team has played its home matches in the 81,044-capacity Santiago Bernabéu Stadium in downtown Madrid since 1947.'
    }
]
  }
  render(){
    return(
      <div>
        {
            this.state.clubs.map(club=>(
              <div>
                    <Club Tittle={club.Tittle} Description={club.Description}/>
            </div>
          ))
        }
    </div>  
    )
  }
}

export default Clubs