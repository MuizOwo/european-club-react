import React from 'react';
import Clubs from './Component/Clubs/Clubs';
import Add from './Component/AddClub/Add'
import Uefa from './Component/uefa'



function App() {
  return (
    <div className="container">
          <div className='row'>
            <div className='col m8 s12'>
              <Clubs />
            </div>
            <div className='col m4 s12'>
              <div className='section'>
                <Add />
              </div>
              <div className="divider"></div>
              <div className='section'>
                <Uefa />
              </div>
            </div>
          </div>
    </div>
  );
}

export default App;
